var t1 = document.getElementById("t1");
var t2 = document.getElementById("t2");
var t3 = document.getElementById("t3");
function add() {
    var c = parseFloat(t1.value) + parseFloat(t2.value);
    t3.value = c.toString();
}
function sub() {
    var c = parseFloat(t1.value) - parseFloat(t2.value);
    t3.value = c.toString();
}
function mul() {
    var c = parseFloat(t1.value) * parseFloat(t2.value);
    t3.value = c.toString();
}
function div() {
    var c = parseFloat(t1.value) / parseFloat(t2.value);
    t3.value = c.toString();
}
function sqrt() {
    var temp = Math.pow(+t1.value, 2);
    t3.value = temp.toString();
}
function root() {
    var temp = Math.sqrt(+t1.value);
    t3.value = temp.toString();
}
function sin() {
    var temp = Math.PI / 180 * (+t1.value);
    var s = Math.sin(temp);
    t3.value = s.toString();
}
function cos() {
    var temp = Math.PI / 180 * (+t1.value);
    var c = Math.cos(temp);
    t3.value = c.toString();
}
function tan() {
    var temp = Math.PI / 180 * (+t1.value);
    var t = Math.sin(temp);
    t3.value = t.toString();
}
//# sourceMappingURL=q4.js.map