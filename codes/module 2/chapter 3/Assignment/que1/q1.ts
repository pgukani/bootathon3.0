function check()
{

    let a :HTMLInputElement = <HTMLInputElement>document.getElementById("t1");
    let ans : HTMLParagraphElement=<HTMLParagraphElement>document.getElementById("ans");
    
    var no : number = +a.value;
    if(isNaN(no)!==true)
    {
        if(no % 2 == 0)
        {
            ans.innerHTML=no + "  is Even";
        }
        else
        {
            ans.innerHTML=no + "  is Odd";
        }
    }
    else
    {
        alert("Enter number in the textbox");
    }
    
}