function real() {
    let x = document.getElementById("t1");
    let y = document.getElementById("t2");
    let z = document.getElementById("t3");
    var data = x.value;
    var real;
    var img;
    var i = data.indexOf("+");
    var j = data.indexOf("-");
    if (i != -1) {
        real = +data.substring(0, i);
        img = +data.substring(i + 1, data.length - 1);
        y.value = "Real part : " + real;
        z.value = "Imaginary part : " + img;
    }
    else if (j != -1) {
        real = +data.substring(0, j);
        img = +data.substring(j + 1, data.length - 1);
        y.value = "Real part : " + real;
        z.value = "Imaginary part : " + img;
    }
    else {
        real = +data.substring(0, data.length);
        y.value = "Real part : " + real;
        z.value = "Imaginary part : 0 ";
    }
}
//# sourceMappingURL=q3.js.map