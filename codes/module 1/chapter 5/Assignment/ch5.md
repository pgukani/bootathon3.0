
**<center><span>Case study explaining function of Fuzzy Inference System</span></center>**

<span style="font-size: 24px;">Aim</span>
<br>To apply fuzzy logic and determine the time required to run a Washing machine, given the amount of dirt and grease on clothes.<br><br>
<span style="font-size: 24px;">Theory</span><br><br>
        Fuzzy logic is basically a multi-valued logic that allows intermediate value to be defined between conventional evaluations like yes/no, true/false and black/white. Notions like a warm cold or very cold can be formulated mathematically and processed by computers.<br>
        <br>
        **Fuzzy inference** (reasoning) is the actual process of mapping from a given input to output using fuzzy logic.<br>
        The process involves all the pieces that we have discussed in the previous sections: membership functions, fuzzy logic operators, and if-then rules.
        <br><br>
        **Fuzzy Interference Systems**
        <br>Fuzzy inference systems have been successfully applied in fields such as automatic control, data classification, decision analysis, expert systems, and computer vision.
         <br>Because of its multi-disciplinary nature, the fuzzy inference system is known by a number of names, such as fuzzy-rule-based system, fuzzy expert system, fuzzy model, fuzzy associative memory, fuzzy logic controller, and simply fuzzy system.
        <br><br>**The steps of fuzzy reasoning performed by FISs are:**<br>
        <br>
1. Compare the input variables with the membership functions on the antecedent part to obtain the membership values of each linguistic label. (this step is often called fuzzification.<br>
1. Combine (usually multiplication or min) the membership values on the premise part to get firing strength (degree of fulfilment) of each rule.<br>
1. Generate the qualified consequents (either fuzzy or crisp) or each rule depending on the firing strength.<br>
1. Aggregate the qualified consequents to produce a crisp output. (This step is called defuzzification).
        <br><br>
        **Architecture**
        <br><br>
        ![architecture](img1.png)
        <br><br>
        **Fuzzy Rule Base:** Rule base that contains a number of fuzzy rules.<br>
        **Fuzzification:** A process to convert the crisp input to a linguistic variable using the membership functions stored in the fuzzy.<br>
        **Inference Engine:** Using fuzzy rules converts the fuzzy input to the fuzzy output.<br>
        **Defuzzification:** A process to convert the fuzzy output of the inference engine to a crisp using membership functions analogous to the ones used by the fuzzifier.<br><br>
        Here we will be using the **washing machine** which is a common feature in a typical Indian household.<br>
        We will formulate a precise mathematical relationship between the amount of grease, dirt and the duration of washing time required.<br>
        The input parameters used to solve the above mention problem are:<br><br>
+ grease
+ dirt
        <br><br>
        The fuzzy controller takes two inputs, processes the information and gives output as washing time.<br>
        The two crisp inputs, grease and dirtiness vary from 0 to 100 and presented as fuzzy sets defined by their respective membership functions. Let the output: washing time be allowed to have three linguistic values less, medium and high. Similarly, let the input variable: grease be expressed as low, average and large and dirtiness of clothes be described as being less, medium and high.
        <br><br>**Rules**<br>
        <br>The decision which the fuzzy controller makes is derived from the rules which are stored in the database. These are stored in the set of rules. Basically, the rules are if-then statements that are intuitive and easy to understand, since they are nothing but common English statements. For example, the set of rules that can be used to derive the output:
        <br><br>
1. if(dirtiness is less) and (grease is low) then (washing time is less).<br>
1. if(dirtiness is less) and (grease is average) then (washing time is less).<br>
1. if (dirtiness is less) and (grease is large) then (washing time is medium).<br>
1. if (dirtiness is medium) and (grease is low) then (washing time is less).<br>
1. if (dirtiness is medium) and (grease is average) then (washing- time is medium)<br>
1. if dirtiness is medium) and (grease is large) then (washing time is high)<br>
1. if (dirtiness is high) and (grease is low) then (washing time is medium). <br>
1. if (dirtiness is high) and (grease is medium) then (washing time is high).<br>
1. if(dirtiness is less) and (grease is high) then (washing time is high).<br>
        <br><br>
        The rules to have been defined in an imprecise sense and hence they too are not crisp but fuzzy values. The two input parameters after being read from the sensors are fuzzified as per the membership function of the respective variables. At last the crisp value of washing time is obtained as an answer.
        <br><br><span style="font-size: 24px;">Procedure</span><br><br>
        The Procedures to be followed for the simulation are:<br>
        **Step 1**<br>
            1. We can edit the descriptor name, start and end value of the descriptor. Also, we can add more descriptors upto a total count of 4. The maximum value for greese and dirt input is 100% and for the washing time, it is 120 mins.<br>
            2. To see changes in the graph, press the <input type="button" value="View changes" style="background-color: orange;color: white;"> button.<br>
            3. To proceed to the next step, press the <input type="button" value="Continue " style="background-color: green;color: white;">button.<br>
        **step 2**<br>
            1. Fill the table shown by choosing the desired output descriptor for each combination of input descriptors.<br>
            2. If you need to make changes to the descriptors, you can do so by clicking the <input type="button" value="Edit descriptors" style="background-color: orange;color: white;"> button<br>
            3. If you want to proceed to the next step, click the <input type="button" value="proceed " style="background-color: green;color: white;">button.<br>
        **step 3**<br>
        1. Here finally you can provide a grease percent and dirt percent as input, and get the required wash time as output.<br>
            2. If you want any changes to be made to the inference table, you can do so by clicking the <input type="button" value=" Edit Inference" style="background-color: orange;color: white;"> Table button.<br>
            3. Once you are completely sure regarding the inference table and descriptors, you can click the Find <input type="button" value="wash Time " style="background-color: green;color: white;"> button to get your wash time..<br>
            4. Now the defuzzified output is calculated using the centroid method which would require the calculation of areas and centroids for all descriptors. The display of calculations involved is displayed in a sequential manner which explains the defuzzification process and the formula involved. Finally, the output wash time is displayed as the output.<br>
        
        

        
        
       