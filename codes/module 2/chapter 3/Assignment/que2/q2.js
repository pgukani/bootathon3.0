function area() {
    let x = document.getElementById("t1");
    let y = document.getElementById("t2");
    let z = document.getElementById("t3");
    let ans = document.getElementById("ans");
    var a = +x.value;
    var b = +y.value;
    var c = +z.value;
    if (a == b && b == c) {
        ans.innerHTML = "Triangle is equilateral";
    }
    else if ((a == b && a != c) || (b == c && b != a) || (a == c && c != b)) {
        ans.innerHTML = "Triangle is isosceles";
        if (Math.pow(a, 2) == (Math.pow(b, 2) + Math.pow(c, 2)) || Math.pow(b, 2) == (Math.pow(a, 2) + Math.pow(c, 2)) || Math.pow(c, 2) == (Math.pow(a, 2) + Math.pow(b, 2))) {
            ans.innerHTML = "Triangle is isosceles and right angle triangle";
        }
    }
    else if (a != b && b != c && a != c) {
        ans.innerHTML = "Triangle is scalene";
        if (Math.pow(a, 2) == (Math.pow(b, 2) + Math.pow(c, 2)) || Math.pow(b, 2) == (Math.pow(a, 2) + Math.pow(c, 2)) || Math.pow(c, 2) == (Math.pow(a, 2) + Math.pow(b, 2))) {
            ans.innerHTML = "Triangle is isosceles and right angle triangle";
        }
    }
}
//# sourceMappingURL=q2.js.map